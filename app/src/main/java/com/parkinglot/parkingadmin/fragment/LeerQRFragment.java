package com.parkinglot.parkingadmin.fragment;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.util.SparseArray;
import android.view.LayoutInflater;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.URLUtil;
import android.widget.TextView;
import android.widget.Toast;

import androidx.core.app.ActivityCompat;
import androidx.fragment.app.Fragment;

import com.google.android.gms.vision.CameraSource;
import com.google.android.gms.vision.Detector;
import com.google.android.gms.vision.barcode.Barcode;
import com.google.android.gms.vision.barcode.BarcodeDetector;
import com.parkinglot.parkingadmin.R;
import com.parkinglot.parkingadmin.model.Clases.VehiculoParqueado;
import com.parkinglot.parkingadmin.model.Modelo;

import java.io.IOException;

public class LeerQRFragment extends Fragment{


    Modelo modelo = Modelo.getInstance();
    private CameraSource cameraSource;
    private SurfaceView cameraView;
    private final int MY_PERMISSIONS_REQUEST_CAMERA = 1;
    private String token = "";
    private String tokenanterior = "";
    String dataQr="";

    TextView txtqr;

    public LeerQRFragment() {
        // Required empty public constructor
    }

    public static LeerQRFragment newInstance(String param1, String param2) {
        LeerQRFragment fragment = new LeerQRFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_qr, container, false);

        cameraView = (SurfaceView) view.findViewById(R.id.camera_view);
        txtqr = (TextView) view.findViewById(R.id.txtqr);
        initQR();
        return view;
    }


    public void initQR() {

        // creo el detector qr
        BarcodeDetector barcodeDetector =
                new BarcodeDetector.Builder(getActivity())
                        .setBarcodeFormats( Barcode.ALL_FORMATS)
                        .build();

        // creo la camara
        cameraSource = new CameraSource
                .Builder(getActivity(), barcodeDetector)
                .setRequestedPreviewSize(1600, 1024)
                .setAutoFocusEnabled(true) //you should add this feature
                .build();

        // listener de ciclo de vida de la camara
        cameraView.getHolder().addCallback(new SurfaceHolder.Callback() {
            @Override
            public void surfaceCreated(SurfaceHolder holder) {

                // verifico si el usuario dio los permisos para la camara
                if (ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.CAMERA)
                        != PackageManager.PERMISSION_GRANTED) {

                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                        // verificamos la version de ANdroid que sea al menos la M para mostrar
                        // el dialog de la solicitud de la camara
                        if (shouldShowRequestPermissionRationale(
                                Manifest.permission.CAMERA)) ;
                        requestPermissions(new String[]{Manifest.permission.CAMERA},
                                MY_PERMISSIONS_REQUEST_CAMERA);
                    }
                    return;
                } else {
                    try {
                        cameraSource.start(cameraView.getHolder());
                    } catch (IOException ie) {
                        Log.e("CAMERA SOURCE", ie.getMessage());
                    }
                }
            }

            @Override
            public void surfaceChanged(SurfaceHolder holder, int format, int width, int height) {
            }

            @Override
            public void surfaceDestroyed(SurfaceHolder holder) {
                cameraSource.stop();
            }
        });

        // preparo el detector de QR
        barcodeDetector.setProcessor(new Detector.Processor<Barcode>() {
            @Override
            public void release() {
            }


            @Override
            public void receiveDetections(Detector.Detections<Barcode> detections) {
                final SparseArray<Barcode> barcodes = detections.getDetectedItems();

                if (barcodes.size() > 0) {

                    getActivity().runOnUiThread(new Runnable() {
                        @Override
                        public void run() {

                            try{
                                token = barcodes.valueAt(0).displayValue.toString();


                                String  dataqr = token;

                                txtqr.setText( dataqr );
                                String[] parts = dataqr.split("\\|");
                                String part1 = parts[0]; // 123
                                String part2 = parts[1];

                                VehiculoParqueado vehiculoParqueado = new VehiculoParqueado(  );
                                for (int i = 0; i <modelo.listVehiculoParqueado.size(); i++){
                                    if (modelo.listVehiculoParqueado.get( i ).getPlaca().equals( part1 )){
                                        vehiculoParqueado= modelo.listVehiculoParqueado.get( i );
                                        break;
                                    }
                                }

                                if(vehiculoParqueado == null){
                                    Toast.makeText( getActivity(), "Placa no considen", Toast.LENGTH_LONG ).show();
                                }

                                Log.i("vehiculoParqueado",""+ vehiculoParqueado);


                            }catch (Exception e){
                                Log.v( "error", e.getMessage() );
                                Toast.makeText( getActivity(),"Escanea nuevamente", Toast.LENGTH_LONG ).show();
                            }

                            // verificamos que el token anterior no se igual al actual
                            // esto es util para evitar multiples llamadas empleando el mismo token



                        }
                    });

                    // obtenemos el token






                   // if (!token.equals(tokenanterior)) {

                        // guardamos el ultimo token proceado
                        //tokenanterior = token;
                        //Log.i("token", token);

                        //dataQr = dataQr;

                       /* if (URLUtil.isValidUrl(token)) {
                            // si es una URL valida abre el navegador
                            Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(token));
                            startActivity(browserIntent);
                        } else {
                            // comparte en otras apps
                            Intent shareIntent = new Intent();
                            shareIntent.setAction(Intent.ACTION_SEND);
                            shareIntent.putExtra(Intent.EXTRA_TEXT, token);
                            shareIntent.setType("text/plain");
                            startActivity(shareIntent);
                        }*/

                       /* new Thread(new Runnable() {
                            public void run() {
                                try {
                                    synchronized (this) {
                                        wait(5000);
                                        // limpiamos el token
                                        tokenanterior = "";
                                    }
                                } catch (InterruptedException e) {
                                    // TODO Auto-generated catch block
                                    Log.e("Error", "Waiting didnt work!!");
                                    e.printStackTrace();
                                }
                            }
                        }).start();*/

                   /// }
                }
            }
        });

    }
}
