package com.parkinglot.parkingadmin.fragment;

import android.Manifest;
import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.Point;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.text.TextUtils;
import android.util.Log;
import android.util.SparseArray;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.core.app.ActivityCompat;
import androidx.core.app.ShareCompat;
import androidx.core.content.FileProvider;
import androidx.fragment.app.Fragment;

import com.google.android.gms.vision.CameraSource;
import com.google.android.gms.vision.Detector;
import com.google.android.gms.vision.text.TextBlock;
import com.google.android.gms.vision.text.TextRecognizer;
import com.google.zxing.WriterException;
import com.parkinglot.parkingadmin.R;
import com.parkinglot.parkingadmin.model.Comandos.ComandoParqueadero;
import com.parkinglot.parkingadmin.model.Modelo;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Objects;
import java.util.Random;

import androidmads.library.qrgenearator.QRGContents;
import androidmads.library.qrgenearator.QRGEncoder;
import cn.pedant.SweetAlert.SweetAlertDialog;

import static android.content.Context.WINDOW_SERVICE;

public class RegistrarVehiculosFragment extends Fragment implements ComandoParqueadero.OnParqueaderosChangeListener {


    LinearLayout layoutqr, layoutaddvehiculo, layputcamara, layouttexview;
    SurfaceView cameraView;
    TextView fechahora;
    EditText textView;
    CardView scanear;
    ImageView scanearimg, idIVQrcode;
    Button idBtnGenerateQR, btnvolver, btncompartir, btnimprimir;
    Bitmap bitmap;
    QRGEncoder qrgEncoder;
    CameraSource cameraSource;
    final int RequestCameraPermissionID = 1001;
    Modelo modelo = Modelo.getInstance();
    String fecha = "";
    String timeStamp;
    String path;
    String fname;
    private static String APP_DIRECTORY = "parqueaderoQR/";
    String TAG = RegistrarVehiculosFragment.class.getSimpleName();
    EditText btnopcion1, btnopcion2, btnopcion3, btnopcion4, btnopcion5;
    Button iniciar, parar;
    String dataVehiculo;
    ComandoParqueadero comandoParqueadero;
    SweetAlertDialog pDialog;

    public RegistrarVehiculosFragment() {
        // Required empty public constructor
    }

    public static RegistrarVehiculosFragment newInstance(String param1, String param2) {
        RegistrarVehiculosFragment fragment = new RegistrarVehiculosFragment();
        Bundle args = new Bundle();
        fragment.setArguments( args );
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate( savedInstanceState );

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment

        View view = inflater.inflate( R.layout.fragment_registrar_vehiculos, container, false );

        //return inflater.inflate(R.layout.fragment_registrar_vehiculos, container, false);

        cameraView = view.findViewById( R.id.surface_camera_preview );
        textView = view.findViewById( R.id.tv_result );
        scanear = view.findViewById( R.id.scanear );
        scanearimg = view.findViewById( R.id.scanearimg );
        fechahora = view.findViewById( R.id.fechahora );
        layoutqr = view.findViewById( R.id.layoutqr );
        layputcamara = view.findViewById( R.id.layputcamara );
        layouttexview = view.findViewById( R.id.layouttexview );
        layoutaddvehiculo = view.findViewById( R.id.layoutaddvehiculo );
        idBtnGenerateQR = view.findViewById( R.id.idBtnGenerateQR );
        idIVQrcode = view.findViewById( R.id.idIVQrcode );
        btnvolver = view.findViewById( R.id.btnvolver );
        btncompartir = view.findViewById( R.id.btncompartir );
        btnimprimir = view.findViewById( R.id.btnimprimir );
        btnopcion1 = view.findViewById( R.id.btnopcion1 );
        btnopcion2 = view.findViewById( R.id.btnopcion2 );
        btnopcion3 = view.findViewById( R.id.btnopcion3 );
        btnopcion4 = view.findViewById( R.id.btnopcion4 );
        btnopcion5 = view.findViewById( R.id.btnopcion5 );
        iniciar = view.findViewById( R.id.iniciar );
        parar = view.findViewById( R.id.parar );


        comandoParqueadero = new ComandoParqueadero( this );


        parar.setOnClickListener( new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                iniciar.setVisibility( View.VISIBLE );
                parar.setVisibility( View.GONE );
                cameraSource.stop();
            }
        } );

        btnopcion1.setOnClickListener( new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dataVehiculo = btnopcion1.getText().toString();
                Toast.makeText( getActivity(), "Placa selecionada: "+ dataVehiculo, Toast.LENGTH_LONG).show();
            }
        } );

        btnopcion2.setOnClickListener( new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dataVehiculo = btnopcion2.getText().toString();
                Toast.makeText( getActivity(), "Placa selecionada: "+ dataVehiculo, Toast.LENGTH_LONG).show();
            }
        } );

        btnopcion3.setOnClickListener( new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dataVehiculo = btnopcion3.getText().toString();
                Toast.makeText( getActivity(), "Placa selecionada: "+ dataVehiculo, Toast.LENGTH_LONG).show();
            }
        } );


        btnopcion4.setOnClickListener( new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dataVehiculo = btnopcion4.getText().toString();
                Toast.makeText( getActivity(), "Placa selecionada: "+ dataVehiculo, Toast.LENGTH_LONG).show();
            }
        } );


        btnopcion5.setOnClickListener( new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dataVehiculo = btnopcion5.getText().toString();
                Toast.makeText( getActivity(), "Placa selecionada: "+ dataVehiculo, Toast.LENGTH_LONG).show();
            }
        } );



        timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
         path="/storage/emulated/0/parkingqr";
         fname = "qr_"+ timeStamp +".jpg";

        //fecha y hora actual
        Date date =new Date();
        String fechacComplString = modelo.dateFormat.format(date);
        String horaString = modelo.hourFormat.format(date);

        fecha =  fechacComplString + " "+ horaString;
        fechahora.setText(fecha);


        if (Build.VERSION.SDK_INT >= 23) {
            int REQUEST_CODE_CONTACT = 101;
            String[] permissions = {
                    Manifest.permission.WRITE_EXTERNAL_STORAGE};
            // Verify permissions
            for (String str : permissions) {
                if (getActivity().checkSelfPermission(str) != PackageManager.PERMISSION_GRANTED) {
                    //request for access
                    getActivity().requestPermissions(permissions, REQUEST_CODE_CONTACT);

                }
            }
        }


        TextRecognizer textRecognizer = new TextRecognizer.Builder(getActivity()).build();
        if (!textRecognizer.isOperational()) {
            Log.w("MainActivity", "Las dependencias del detector aún no están disponibles");
        } else {

            cameraSource = new CameraSource.Builder(getActivity(), textRecognizer)
                    .setFacing(CameraSource.CAMERA_FACING_BACK)
                    .setRequestedPreviewSize(1280, 1024)
                    .setRequestedFps(5.0f)
                    .setAutoFocusEnabled(true)
                    .build();
            cameraView.getHolder().addCallback(new SurfaceHolder.Callback() {
                @Override
                public void surfaceCreated(SurfaceHolder surfaceHolder) {

                    try {
                        if (ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED) {

                            ActivityCompat.requestPermissions(getActivity(),
                                    new String[]{Manifest.permission.CAMERA},
                                    RequestCameraPermissionID);
                            return;
                        }
                        cameraSource.start(cameraView.getHolder());
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }

                @Override
                public void surfaceChanged(SurfaceHolder surfaceHolder, int i, int i1, int i2) {

                }

                @Override
                public void surfaceDestroyed(SurfaceHolder surfaceHolder) {
                    cameraSource.stop();
                }
            });

            textRecognizer.setProcessor(new Detector.Processor<TextBlock>() {
                @Override
                public void release() {

                }

                @Override
                public void receiveDetections(Detector.Detections<TextBlock> detections) {

                    final SparseArray<TextBlock> items = detections.getDetectedItems();
                    if(items.size() != 0)
                    {
                        textView.post(new Runnable() {
                            @Override
                            public void run() {
                                StringBuilder stringBuilder = new StringBuilder();
                                for(int i =0;i<items.size();++i)
                                {
                                    TextBlock item = items.valueAt(i);
                                    stringBuilder.append(item.getValue());
                                }

                                if(stringBuilder.length() >=5 && stringBuilder.length() <=20){



                                    String resultscaner =  stringBuilder.toString().replace( "peru", "").trim();
                                    resultscaner=  resultscaner.trim();
                                    char[] aCaracteres = resultscaner.toCharArray();


                                    try{

                                        for(int i =0; i <  aCaracteres.length; i++){


                                                if(aCaracteres[0]=='5'){
                                                    aCaracteres[0]='S';
                                                }


                                            if(i > 2){
                                                if(aCaracteres[i]  != ' '){
                                                    if(aCaracteres[i]=='S'){
                                                        aCaracteres[i]='5';
                                                    }
                                                }
                                            }

                                            if( aCaracteres[1]=='S'){

                                                if(aCaracteres[0]=='5'){
                                                    aCaracteres[0]='S';
                                                }

                                                aCaracteres[1]='S';
                                                aCaracteres[2]='5';
                                                StringBuffer cadena = new StringBuffer();
                                                for (int x=0;x<aCaracteres.length;x++){
                                                    cadena =cadena.append(aCaracteres[x]);
                                                }
                                                btnopcion1.setText( "PLACA: "+cadena );

                                                if(aCaracteres[0]=='5'){
                                                    aCaracteres[0]='S';
                                                }

                                                aCaracteres[1]='5';
                                                aCaracteres[2]='S';
                                                StringBuffer cadena2 = new StringBuffer();
                                                for (int x=0;x<aCaracteres.length;x++){
                                                    cadena2 =cadena2.append(aCaracteres[x]);
                                                }
                                                btnopcion2.setText("PLACA: "+ cadena2 );


                                                aCaracteres[1]='S';
                                                aCaracteres[2]='S';
                                                StringBuffer cadena3 = new StringBuffer();
                                                for (int x=0;x<aCaracteres.length;x++){
                                                    cadena3 =cadena3.append(aCaracteres[x]);
                                                }
                                                btnopcion3.setText( "PLACA: "+ cadena3 );

                                                if(aCaracteres[0]=='5'){
                                                    aCaracteres[0]='S';
                                                }
                                                aCaracteres[1]='5';
                                                aCaracteres[2]='5';
                                                StringBuffer cadena4 = new StringBuffer();
                                                for (int x=0;x<aCaracteres.length;x++){
                                                    cadena4 =cadena4.append(aCaracteres[x]);
                                                }
                                                btnopcion4.setText("PLACA: "+ cadena4 );

                                            }



                                            if( aCaracteres[2]=='5'){


                                                aCaracteres[1]='S';
                                                aCaracteres[2]='5';
                                                StringBuffer cadena = new StringBuffer();
                                                for (int x=0;x<aCaracteres.length;x++){
                                                    cadena =cadena.append(aCaracteres[x]);
                                                }
                                                btnopcion1.setText( "PLACA: "+cadena );



                                                if(aCaracteres[0]=='5'){
                                                    aCaracteres[0]='S';
                                                }
                                                aCaracteres[1]='5';
                                                aCaracteres[2]='S';
                                                StringBuffer cadena2 = new StringBuffer();
                                                for (int x=0;x<aCaracteres.length;x++){
                                                    cadena2 =cadena2.append(aCaracteres[x]);
                                                }
                                                btnopcion2.setText("PLACA: "+ cadena2 );




                                                aCaracteres[1]='S';
                                                aCaracteres[2]='S';
                                                StringBuffer cadena3 = new StringBuffer();
                                                for (int x=0;x<aCaracteres.length;x++){
                                                    cadena3 =cadena3.append(aCaracteres[x]);
                                                }
                                                btnopcion3.setText( "PLACA: "+ cadena3 );

                                                if(aCaracteres[0]=='5'){
                                                    aCaracteres[0]='S';
                                                }
                                                aCaracteres[1]='5';
                                                aCaracteres[2]='5';
                                                StringBuffer cadena4 = new StringBuffer();
                                                for (int x=0;x<aCaracteres.length;x++){
                                                    cadena4 =cadena4.append(aCaracteres[x]);
                                                }

                                                btnopcion4.setText("PLACA: "+ cadena4 );

                                            }


                                        }
                                            String txtplaca = btnopcion1.getText().toString();
                                        if(txtplaca.equals( "" )){
                                            btnopcion1.setText( "PLACA: "+resultscaner );
                                            btnopcion2.setText( "PLACA: "+resultscaner );
                                            btnopcion3.setText( "PLACA: "+resultscaner );
                                            btnopcion4.setText( "PLACA: "+resultscaner );

                                        }
                                        btnopcion5.setText("PLACA : "+  resultscaner );



                                    }
                                    catch (Exception e){
                                        Log.v( "Error",e.getMessage() );
                                    }

                                    //textView.setText(stringBuilder.toString());
                                    //dataVehiculo = stringBuilder.toString();
                                }

                            }
                        });
                    }
                }
            });
        }



        //mostrar escaner
        scanearimg.setOnClickListener( new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                scanearimg.setVisibility(View.GONE);
                scanear.setVisibility(View.VISIBLE);
                layouttexview.setVisibility( View.GONE );
                layputcamara.setVisibility( View.VISIBLE );

            }
        } );

        //ocultar escaner
        scanear.setOnClickListener( new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                scanearimg.setVisibility(View.VISIBLE);
                scanear.setVisibility(View.GONE);

                layouttexview.setVisibility( View.VISIBLE );
                layputcamara.setVisibility( View.GONE );
            }
        } );






        ///QR
        idBtnGenerateQR.setOnClickListener( new View.OnClickListener() {
            @SuppressLint("WrongConstant")
            @Override
            public void onClick(View view) {

                try {

                    if(layputcamara.getVisibility() ==  View.VISIBLE){
                        if (TextUtils.isEmpty(dataVehiculo)) {
                            //if the edittext inputs are empty then execute this method showing a toast message.
                            Toast.makeText(getActivity(), "Ingrese la placa  para generar un código QR", Toast.LENGTH_SHORT).show();
                            alert();
                        }else{

                            registrarVehiculo(dataVehiculo,fecha,modelo.parqueadero.getNombreParqueadero() );

                        }
                    }

                    else if(layputcamara.getVisibility() ==  View.GONE){
                        if (TextUtils.isEmpty(textView.getText().toString())) {
                            //if the edittext inputs are empty then execute this method showing a toast message.
                            Toast.makeText(getActivity(), "Ingrese la placa  para generar un código QR", Toast.LENGTH_SHORT).show();
                            alert();
                        }else{

                            String _dataVehiculo = "PLACA: "+textView.getText().toString();
                            registrarVehiculo(_dataVehiculo,fecha,modelo.parqueadero.getNombreParqueadero() );

                        }
                    }

                }catch (Exception ex){
                    Log.v( "Error", ex.getMessage() );


                }
            }
        } );


        //volver
        btnvolver.setOnClickListener( new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                layoutaddvehiculo.setVisibility( View.VISIBLE );
                layoutqr.setVisibility( View.GONE );
                textView.setText( "" );
            }
        } );

        //boton compartir

        btncompartir.setOnClickListener( new View.OnClickListener() {
            @Override

            public void onClick(View view) {

                 //Se guarda la imagen en la SDCARD
             /*   ByteArrayOutputStream bytes = new ByteArrayOutputStream();
                bitmap.compress(Bitmap.CompressFormat.JPEG, 100, bytes);
                File file = new File( Environment.getExternalStorageDirectory() + File.separator +
                        "parkingqr" + File.separator + fname);



                //File f = new File( path+"/"+fname);
                try {
                    file.createNewFile();
                    FileOutputStream fo = new FileOutputStream(file);
                    fo.write(bytes.toByteArray());
                } catch (IOException e) {
                    Log.e("ERROR", e.getMessage() );
                }
                //compartir imagen
               try {
                   Intent share = new Intent(Intent.ACTION_SEND);
                   share.setType("image/*");
                   share.putExtra(Intent.EXTRA_STREAM, Uri.fromFile(file));
                   share.putExtra(Intent.EXTRA_TEXT, "RQ FACTURA");
                   startActivity(Intent.createChooser(share, "Compartir QR"));
               }
               catch (Exception e){
                   Log.e("ERROR", e.getMessage() );
               }*/

             try {
                 Intent shareIntent = new Intent();
                 String path = MediaStore.Images.Media.insertImage(getActivity().getContentResolver(), bitmap, "Image Description", null);
                 Uri uri = Uri.parse(path);
                 shareIntent.setAction(Intent.ACTION_SEND);
                 shareIntent.putExtra(Intent.EXTRA_STREAM, uri);
                 shareIntent.setType("image/jpeg");
                 startActivity(Intent.createChooser(shareIntent, getResources().getText(R.string.app_name)));

             }
             catch (Exception e){
                    Log.v( "error share" , e.getMessage() );
             }
            }
        } );






        //imrimir
        btnimprimir.setOnClickListener( new View.OnClickListener() {
            @Override
            public void onClick(View view) {



            }
        } );
        return  view;
    }



    public void alert(){
        new SweetAlertDialog(getActivity(), SweetAlertDialog.WARNING_TYPE)
                .setTitleText("Placa")
                .setContentText("Ingrese la placa  para generar un código QR")
                .setConfirmText("Aceptar")
                .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                    @Override
                    public void onClick(SweetAlertDialog sDialog) {

                        sDialog.dismissWithAnimation();

                    }
                })

                .show();
    }


    //alerta
    public void _alerta(String titulo, String decripcion) {
        new SweetAlertDialog( getActivity(), SweetAlertDialog.WARNING_TYPE )
                .setTitleText( titulo )
                .setContentText( decripcion )
                .setConfirmText( "Aceptar" )
                .setConfirmClickListener( new SweetAlertDialog.OnSweetClickListener() {
                    @Override
                    public void onClick(SweetAlertDialog sDialog) {

                        sDialog.dismissWithAnimation();

                    }
                } )

                .show();
    }



    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        switch (requestCode) {
            case RequestCameraPermissionID: {
                if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {

                   // checkFile();
                    if (ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED) {
                        return;
                    }
                    try {
                        cameraSource.start(cameraView.getHolder());
                    } catch (IOException e) {
                        e.printStackTrace();
                    }

                }
            }
            break;
        }
    }

    public void savebitmap(Bitmap bitmap) {
        if (isExternalStorageWritable()) {
            saveImage(bitmap);
        }else{
            //prompt the user or do something
        }
    }

    private void saveImage(Bitmap finalBitmap) {

        final String path2 = Environment.DIRECTORY_DOWNLOADS;

        //String root = Environment.getExternalStorageDirectory().toString();
        File root = android.os.Environment.getExternalStorageDirectory();
        File myDir = new File(path);

       // File myDir = new File(dir + "/saved_images");
        myDir.mkdirs();




        File file = new File(myDir, fname);
        if (file.exists()) file.delete ();
        try {
            FileOutputStream out = new FileOutputStream(file);
            finalBitmap.compress(Bitmap.CompressFormat.JPEG, 100, out);
            out.flush();
            out.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /* Checks if external storage is available for read and write */
    public boolean isExternalStorageWritable() {
        String state = Environment.getExternalStorageState();
        if (Environment.MEDIA_MOUNTED.equals(state)) {
            return true;
        }
        return false;
    }



    //imagen

    // Returns the URI path to the Bitmap displayed in specified ImageView
    public Uri getLocalBitmapUri(ImageView imageView) {
        // Extract Bitmap from ImageView drawable
        Drawable drawable = imageView.getDrawable();
        Bitmap bmp = null;
        if (drawable instanceof BitmapDrawable){
            bmp = ((BitmapDrawable) imageView.getDrawable()).getBitmap();
        } else {
            return null;
        }
        // Store image to default external storage directory
        Uri bmpUri = null;
        try {
            File root = android.os.Environment.getExternalStorageDirectory();
            File file = new File(path+"/"+fname);
            FileOutputStream out = new FileOutputStream(file);
            bmp.compress(Bitmap.CompressFormat.PNG, 90, out);
            out.close();
            // **Warning:** This will fail for API >= 24, use a FileProvider as shown below instead.
            bmpUri = Uri.fromFile(file);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return bmpUri;
    }

    // Method when launching drawable within Glide
    public Uri getBitmapFromDrawable(Bitmap bmp){

        // Store image to default external storage directory
        Uri bmpUri = null;
        try {


            File root = android.os.Environment.getExternalStorageDirectory();
            File file = new File(path+"/"+fname);

            FileOutputStream out = new FileOutputStream(file);
            bmp.compress(Bitmap.CompressFormat.PNG, 90, out);
            out.close();

            // wrap File object into a content provider. NOTE: authority here should match authority in manifest declaration
            bmpUri = FileProvider.getUriForFile(getActivity(), "com.parkinglot.parkingadmin", file);  // use this version for API >= 24

            // **Note:** For API < 24, you may use bmpUri = Uri.fromFile(file);

        } catch (IOException e) {
            e.printStackTrace();
        }
        return bmpUri;
    }
    //fin


    //option 2 img

    private void checkFile(){

        //Path directory of the file we want to load.
        File documentsPath = new File(Environment.getExternalStorageDirectory().getPath() + "/Android/Data/");
        //If documentsPath doesn´t exists, then create
        if (!documentsPath.exists()) {
            Log.i(TAG, "create path: " + documentsPath);
            documentsPath.mkdir();
        }else{
            Log.i(TAG, "path: " + documentsPath + " exists!");
        }
        File file = new File(documentsPath, "test.pdf");

        if(file.exists()) {
            Log.i(TAG, "The file exists!, share file.");
            shareFile(file);
        }else{
            Log.e(TAG, "The file doesn´t exists!");
        }


    }

    private void shareFile(File file){

        Uri uri = FileProvider.getUriForFile(getActivity(), "com.parkinglot.parkingadmin" + ".provider", file);

        Intent intent = ShareCompat.IntentBuilder.from(getActivity())
                .setType("application/pdf")
                .setStream(uri)
                .setChooserTitle("Choose bar")
                .createChooserIntent()
                .addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);

        startActivity(intent);
    }


    private static boolean hasPermissions(Context context, String... permissions) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M && context != null && permissions != null) {
            for (String permission : permissions) {
                if (ActivityCompat.checkSelfPermission(context, permission) != PackageManager.PERMISSION_GRANTED) {
                    return false;
                }
            }
        }
        return true;
    }





    //registarVehiculo

    public  void registrarVehiculo(String placa, String fecha,String nombreParqueadero){


        loadswet( "Validando la información..." );

        generarQR(placa,fecha);
        comandoParqueadero.registrarVehiculo(placa,fecha, nombreParqueadero );
    }



    public void loadswet(String text) {

        try {
            pDialog = new SweetAlertDialog( getActivity(), SweetAlertDialog.PROGRESS_TYPE );
            pDialog.getProgressHelper().setBarColor( Color.parseColor( "#A5DC86" ) );
            pDialog.setTitleText( text );
            pDialog.setCancelable( false );
            pDialog.show();

        } catch (Exception e) {

        }

    }
    //oculatomos el dialog
    private void hideDialog() {
        if (pDialog != null)
            pDialog.dismiss();
    }



    public void generarQR(String _dataVehiculo, String _fecha){


        //below line is for getting the windowmanager service.
        WindowManager manager = (WindowManager) getActivity().getSystemService(WINDOW_SERVICE);
        //initializing a variable for default display.
        Display display = manager.getDefaultDisplay();
        //creating a variable for point which is to be displayed in QR Code.
        Point point = new Point();
        display.getSize(point);
        //getting width and height of a point
        int width = point.x;
        int height = point.y;
        //generating dimension from width and height.
        int dimen = width < height ? width : height;
        dimen = dimen * 3 / 4;
        //setting this dimensions inside our qr code encoder to generate our qr code.
        String dataQR = _dataVehiculo+"|"+_fecha;
        qrgEncoder = new QRGEncoder(dataQR, null, QRGContents.Type.TEXT, dimen);
        try {
            //getting our qrcode in the form of bitmap.
            bitmap = qrgEncoder.encodeAsBitmap();
            // the bitmap is set inside our image view using .setimagebitmap method.
            idIVQrcode.setImageBitmap(bitmap);


            //guardar imagen

            try{

                savebitmap( bitmap );
            } catch (Exception e) {
                e.printStackTrace();
            }

        } catch (WriterException e) {
            //this method is called for exception handling.
            Log.e("Tag", e.toString());
        }

        layoutaddvehiculo.setVisibility( View.GONE );
        layoutqr.setVisibility( View.VISIBLE );
    }

    @Override
    public void cargoValidarCorreoFirebase() {

    }

    @Override
    public void cargoValidarCorreoFirebaseEroor() {

    }

    @Override
    public void errorCreacionParqueadero() {

    }

    @Override
    public void setParqueaderoListener() {

    }

    @Override
    public void errorSetParqueadero() {

    }

    @Override
    public void setParueaderoListener() {

    }

    @Override
    public void cargoParqueadero() {

    }

    @Override
    public void addParueaderoListener() {


        hideDialog();
        _alerta( "Parqueadero","Vehiculo registrado correctamente" );

    }
}
