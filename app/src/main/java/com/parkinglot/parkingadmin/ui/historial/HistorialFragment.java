package com.parkinglot.parkingadmin.ui.historial;

import android.graphics.Color;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.SearchView;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.parkinglot.parkingadmin.R;
import com.parkinglot.parkingadmin.adapter.ListVehiculoAdapter;
import com.parkinglot.parkingadmin.adapter.ListVehiculoHistorialAdapter;
import com.parkinglot.parkingadmin.model.Comandos.ComandoListaVehiculoParqueado;
import com.parkinglot.parkingadmin.model.Modelo;
import com.parkinglot.parkingadmin.model.utility.Utility;

import cn.pedant.SweetAlert.SweetAlertDialog;

public class HistorialFragment extends Fragment  implements ComandoListaVehiculoParqueado.OnValidarListaVehiculoParqueadoChangeListener {

    private HistorialViewModel galleryViewModel;
    Modelo modelo = Modelo.getInstance();
    ComandoListaVehiculoParqueado comandoListaVehiculoParqueado;

    Utility utility;
    SweetAlertDialog pDialog;
    private static int splashTimeOut=5000;
    ListVehiculoHistorialAdapter listVehiculoAdapter;
    RecyclerView recyclerView;


    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        galleryViewModel =
                ViewModelProviders.of( this ).get( HistorialViewModel.class );
        View root = inflater.inflate( R.layout.fragment_historial, container, false );


        comandoListaVehiculoParqueado = new ComandoListaVehiculoParqueado( this );
        final SearchView search2 = (SearchView) root.findViewById(R.id.search_view);
        recyclerView = root.findViewById(R.id.recycler_view2);

        recyclerView.setNestedScrollingEnabled(true);
        utility = new Utility();

        if (utility.estado(getActivity())) {
            loadswet("Cargando información...");
            new Handler().postDelayed( new Runnable() {
                @Override
                public void run() {

                    comandoListaVehiculoParqueado.historialListaVhiculoParqueado();

                }
            },splashTimeOut);

        }else{
            alerta("Sin Internet","Valide la conexión a internet");
        }




        search2.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String queryString) {


                listVehiculoAdapter.getFilter().filter(queryString);


                return false;
            }

            @Override
            public boolean onQueryTextChange(String queryString) {

                listVehiculoAdapter.getFilter().filter(queryString);

                return false;
            }
        });


        return root;
    }



    //alerta
    public void alerta(String titulo,String decripcion){

        new SweetAlertDialog(getActivity(), SweetAlertDialog.WARNING_TYPE)
                .setTitleText(titulo)
                .setContentText(decripcion)
                .setConfirmText("Aceptar")
                .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                    @Override
                    public void onClick(SweetAlertDialog sDialog) {

                        sDialog.dismissWithAnimation();

                    }
                })

                .show();
    }


    public void loadswet(String text){

        try {
            pDialog = new SweetAlertDialog(getActivity(), SweetAlertDialog.PROGRESS_TYPE);
            pDialog.getProgressHelper().setBarColor( Color.parseColor("#A5DC86"));
            pDialog.setTitleText(text);
            pDialog.setCancelable(false);
            pDialog.show();

        }catch (Exception e){

        }

    }

    //oculatomos el dialog
    private void hideDialog() {
        if (pDialog != null)
            pDialog.dismiss();
    }



    @Override
    public void validandoListaVehiculoParqueadoOK() {

        int cantidad = modelo.listHistoricoVehiculoParqueado.size();
        Log.v("cantidad", ""+cantidad);
        try {

            if(cantidad > 0){

                listVehiculoAdapter = new ListVehiculoHistorialAdapter(getActivity(), modelo.listHistoricoVehiculoParqueado);
                RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false);
                recyclerView.setLayoutManager(layoutManager);
                recyclerView.setAdapter(listVehiculoAdapter);
            }
            hideDialog();
        }catch (Exception e){
            Log.v( "Error", e.getMessage() );
            hideDialog();
        }

    }

    @Override
    public void validandoListaVehiculoParqueadoError() {

    }
}