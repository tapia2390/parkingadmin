package com.parkinglot.parkingadmin.ui.home;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;
import androidx.lifecycle.ViewModelProviders;

import com.google.android.material.bottomnavigation.BottomNavigationView;
import com.parkinglot.parkingadmin.R;
import com.parkinglot.parkingadmin.fragment.LeerQRFragment;
import com.parkinglot.parkingadmin.fragment.ListVehiculoFragment;
import com.parkinglot.parkingadmin.fragment.RegistrarVehiculosFragment;
import com.parkinglot.parkingadmin.model.helper.BottomNavigationBehavior;
import com.parkinglot.parkingadmin.model.utility.Utility;

public class HomeFragment extends Fragment {

    Utility utility;

    private HomeViewModel homeViewModel;

    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        homeViewModel =
                ViewModelProviders.of( this ).get( HomeViewModel.class );
        View root = inflater.inflate( R.layout.fragment_home, container, false );
       // final TextView textView = root.findViewById( R.id.text_home );
      /*  homeViewModel.getText().observe( getViewLifecycleOwner(), new Observer<String>() {
            @Override
            public void onChanged(@Nullable String s) {

                //textView.setText( s );
            }
        } );*/

        BottomNavigationView navigation = root.findViewById(R.id.navigation);
        navigation.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener);

        // attaching bottom sheet behaviour - hide / show on scroll
       // CoordinatorLayout.LayoutParams layoutParams = (CoordinatorLayout.LayoutParams) navigation.getLayoutParams();
        //layoutParams.setBehavior(new BottomNavigationBehavior());

        // load the store fragment by default

        loadFragment(new ListVehiculoFragment());

        utility = new Utility();

        utility.checkPermissionCamera(getActivity());
        return root;
    }


    private BottomNavigationView.OnNavigationItemSelectedListener mOnNavigationItemSelectedListener
            = new BottomNavigationView.OnNavigationItemSelectedListener() {

        @Override
        public boolean onNavigationItemSelected(@NonNull MenuItem item) {
            Fragment fragment;
            switch (item.getItemId()) {

                case R.id.navigation_lista_vehiculo:

                    fragment = new ListVehiculoFragment();
                    loadFragment(fragment);
                    return true;
                case R.id.navigation_registro_vehiculo:
                    fragment = new RegistrarVehiculosFragment();
                    loadFragment(fragment);
                    return true;


                case R.id.navigation_leer_qr:
                    fragment = new LeerQRFragment();
                    loadFragment(fragment);
                    return true;


            }

            return false;
        }
    };

    /**
     * loading fragment into FrameLayout
     *
     * @param fragment
     */
    private void loadFragment(Fragment fragment) {
        // load fragment
        FragmentTransaction transaction = getActivity().getSupportFragmentManager().beginTransaction();
        transaction.replace(R.id.frame_container, fragment);
        transaction.addToBackStack(null);
        transaction.commit();
    }

}