package com.parkinglot.parkingadmin.ui.perfil;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;

import com.parkinglot.parkingadmin.R;
import com.parkinglot.parkingadmin.model.Comandos.ComandoPerfilFragment;
import com.parkinglot.parkingadmin.model.Comandos.ComandoValidarCorreoFirebase;
import com.parkinglot.parkingadmin.model.Modelo;
import com.parkinglot.parkingadmin.model.utility.Utility;

import cn.pedant.SweetAlert.SweetAlertDialog;

public class PerfilFragment extends Fragment implements ComandoPerfilFragment.OnPerfilChangeListener {

    Modelo modelo = Modelo.getInstance();
    EditText name, mobile, lastname, email, password, comfirmpassword;
    private PerfilViewModel galleryViewModel;
    Button update;
    Utility utility;
    ComandoPerfilFragment comandoPerfilFragment;

    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        galleryViewModel =
                ViewModelProviders.of( this ).get( PerfilViewModel.class );
        View root = inflater.inflate( R.layout.fragment_perfil, container, false );

        name = (EditText) root.findViewById( R.id.name );
        mobile = (EditText) root.findViewById( R.id.mobile );
        lastname = (EditText) root.findViewById( R.id.lastname );
        email = (EditText) root.findViewById( R.id.email );
        update = (Button) root.findViewById( R.id.update );


        utility = new Utility();

        name.setText( modelo.usuario.getNombre() );
        mobile.setText( modelo.usuario.getCelular() );
        lastname.setText( modelo.usuario.getApellido());
        email.setText( modelo.usuario.getCorreo());


        comandoPerfilFragment = new ComandoPerfilFragment( this );

        update.setOnClickListener( new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (name.getText().toString().length() < 3) {
                    name.setError( "nombre muy corta" );
                } else if (mobile.getText().toString().length() < 3) {
                    mobile.setError( "celular muy corto" );
                } else if (lastname.getText().toString().length() < 3) {
                    lastname.setError( "apellido muy corto" );
                }  else {

                    if (utility.estado( getActivity() )) {

                        String nombre =name.getText().toString();
                        String apellido =lastname.getText().toString();
                        String celular =mobile.getText().toString();
                        comandoPerfilFragment.actualizarUsuario(nombre, apellido, celular);


                    } else {
                        alerta( "Sin Internet", "Valide la conexión a internet" );
                    }

                }
            }
        } );

        return root;
    }
    //alerta
    public void alerta(String titulo, String decripcion) {
        new SweetAlertDialog( getActivity(), SweetAlertDialog.WARNING_TYPE )
                .setTitleText( titulo )
                .setContentText( decripcion )
                .setConfirmText( "Aceptar" )
                .setConfirmClickListener( new SweetAlertDialog.OnSweetClickListener() {
                    @Override
                    public void onClick(SweetAlertDialog sDialog) {

                        sDialog.dismissWithAnimation();

                    }
                } )

                .show();
    }

    @Override
    public void cargoUSuario() {

    }

    @Override
    public void setUsuarioListener() {

        alerta( "Actualizacón","Datos actualizdos con éxito" );
    }

    @Override
    public void errorSetUsuario() {

    }
}

