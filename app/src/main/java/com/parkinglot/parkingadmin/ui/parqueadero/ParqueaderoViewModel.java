package com.parkinglot.parkingadmin.ui.parqueadero;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

public class ParqueaderoViewModel extends ViewModel {

    private MutableLiveData<String> mText;

    public ParqueaderoViewModel() {
        mText = new MutableLiveData<>();
        mText.setValue( "This is slideshow fragment" );
    }

    public LiveData<String> getText() {
        return mText;
    }
}