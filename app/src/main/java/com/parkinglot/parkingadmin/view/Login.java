package com.parkinglot.parkingadmin.view;

import androidx.annotation.NonNull;
import androidx.core.app.ActivityCompat;

import android.Manifest;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.location.LocationProvider;
import android.os.Bundle;
import android.provider.Settings;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.android.gms.vision.CameraSource;
import com.google.firebase.FirebaseNetworkException;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseAuthException;
import com.google.firebase.auth.FirebaseUser;
import com.parkinglot.parkingadmin.MainActivity;
import com.parkinglot.parkingadmin.R;
import com.parkinglot.parkingadmin.model.Comandos.ComandoValidarUsuario;
import com.parkinglot.parkingadmin.model.Modelo;
import com.parkinglot.parkingadmin.model.utility.Utility;

import java.io.IOException;
import java.util.List;
import java.util.Locale;

import cn.pedant.SweetAlert.SweetAlertDialog;
public class Login extends Activity implements ComandoValidarUsuario.OnValidarUsuarioChangeListener {


    private FirebaseAuth mAuth;

    String tipoLogin;
    private  String TAG = "MainActivity";
    private int RC_SIGN_IN = 1;

    //auth
    private FirebaseAuth.AuthStateListener mAuthListener;
    private EditText email, password;
    Modelo modelo = Modelo.getInstance();
    Utility utility;
    SweetAlertDialog pDialog;

    CameraSource cameraSource;
    final int RequestCameraPermissionID = 1001;


    //latlong
    /*Se declara una variable de tipo LocationManager encargada de proporcionar acceso al servicio de localización del sistema.*/
    private LocationManager locManager;
    /*Se declara una variable de tipo Location que accederá a la última posición conocida proporcionada por el proveedor.*/
    private Location loc;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_login);

        if (savedInstanceState != null) {
            Intent i = new Intent(getApplicationContext(), MainActivity.class);
            startActivity(i);
            finish();
            return;
        }

        mAuth = FirebaseAuth.getInstance();

        email = (EditText) findViewById(R.id.email);
        password = (EditText) findViewById(R.id.password);

        //instanciamos la clase utility
        utility = new Utility();

        getPreference();
        modelo.tipoLogin = tipoLogin;
        preferences();


        //gps

        mapa();
        latlong();

    }

    public void registrarse(View v) {
        Intent i = new Intent(getApplicationContext(), Inicio.class);
        startActivity(i);
        overridePendingTransition(R.anim.push_down_in, R.anim.push_down_out);
    }

    public void terminos(View v) {
        Intent i = new Intent(getApplicationContext(), Teminos.class);
        startActivity(i);
        overridePendingTransition(R.anim.push_down_in, R.anim.push_down_out);
    }

    public void recuperarContrasena(View v) {
        Intent i = new Intent(getApplicationContext(), OlvidoSuPassword.class);
        startActivity(i);
        overridePendingTransition(R.anim.push_down_in, R.anim.push_down_out);
    }


    public void getPreference(){
        SharedPreferences prefs = getSharedPreferences("tipoLogin", MODE_PRIVATE);
        tipoLogin = prefs.getString("tipoLogin", "");//"No name defined" is the default value.
    }

    public void setPreference(String tipoLogin){
        SharedPreferences.Editor editor = getSharedPreferences("tipoLogin", MODE_PRIVATE).edit();
        editor.putString("tipoLogin", tipoLogin);
        editor.apply();
    }


    //auth
    private void preferences() {


        if (utility.estado(getApplicationContext())) {


            mAuthListener = new FirebaseAuth.AuthStateListener() {
                @Override
                public void onAuthStateChanged(@NonNull FirebaseAuth firebaseAuth) {
                    FirebaseUser user = firebaseAuth.getCurrentUser();
                    if (user != null) {
                        // User is signed in

                        modelo.uid = user.getUid();
                        Log.d(TAG, "onAuthStateChanged:signed_in:" + user.getUid());
                        loadswet("Validando la información...");

                        try {
                            hideDialog();
                        } catch (Exception e){}

                        //catgamos el tipo de usuario logeuado g,f,n

                        logueado();
                    } else {
                        try {
                            hideDialog();
                        } catch (Exception e){}
                        // User is signed out
                        Log.d(TAG, "onAuthStateChanged:signed_out" + user + "no logueado");
                        // Toast.makeText(getApplication(),"Error con los datos registrados.", Toast.LENGTH_SHORT).show();

                    }

                }
            };
        }else {
            alerta("Sin Internet","Valide la conexión a internet");
        }

    }




    @Override
    public void onStart() {
        super.onStart();

        if (mAuth == null || mAuthListener == null) {
            return;
        } else {
            mAuth.addAuthStateListener(mAuthListener);
        }

    }

    @Override
    public void onStop() {
        super.onStop();
        if (mAuthListener != null) {
            mAuth.removeAuthStateListener(mAuthListener);
        }
    }


    public  void validar(View v){
        validarDatos();
    }

    private void validarDatos() {

        String correo = email.getText().toString();
        String password2 = password.getText().toString();

        if (utility.estado(getApplicationContext())) {
            if(correo.equals("") || password2.equals("")){
                alerta("Campos Obligatorios","todos los campos son requeridos!");
            }else if (!utility.validarEmail(correo)){
                alerta("Correo Electronico","Correo Electrónico no válido");
            }
            else if (password2.length()  < 7){
                alerta("Contraseña Incorrecta","La contraseña no es válida.");
            }
            else{

                if (utility.estado(getApplicationContext())) {
                    modelo.tipoLogin = "normal";
                    setPreference(modelo.tipoLogin);
                    Inicio(correo,password2);

                }else{
                    alerta("Sin Internet","Valide la conexión a internet");
                }

            }

        }

    }

    private void Inicio(final String correo, final String password) {

        loadswet("Validando la información...");
        mAuth.signInWithEmailAndPassword(correo,password)
                .addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        Log.d(TAG, "signInWithEmail:onComplete:" + task.isSuccessful());

                        if (task.getException() instanceof FirebaseAuthException) {

                            FirebaseAuthException ex = (FirebaseAuthException) task.getException();

                            if (ex == null) {
                                return;
                            }

                            String error = ex.getErrorCode();

                            if (error.equals("ERROR_INVALID_EMAIL")) {
                                Log.d(TAG, "signInWithEmail:onComplete:" + "correo nada que ver");
                                // Toast.makeText(getApplication(), "...." + "correo nada que ver", Toast.LENGTH_SHORT).show();
                                //showAlertDialogLogin();

                                try {
                                    hideDialog();
                                } catch (Exception e){}

                            }
                            if (error.equals("ERROR_USER_NOT_FOUND")) {
                                Log.d(TAG, "signInWithEmail:onComplete:" + "USUARIO NUEVO");
                                // Toast.makeText(getApplication(), "...." + "USUARIO NUEVO", Toast.LENGTH_SHORT).show();
                                showAlertDialogLogin();

                                try {
                                    hideDialog();
                                } catch (Exception e){}

                            }
                            if (error.equals("ERROR_WRONG_PASSWORD")) {
                                Log.d(TAG, "signInWithEmail:onComplete:" + "CONTRASEÑA INCORRECTA");
                                //Toast.makeText(getApplication(), "...." + "CONTRASEÑA INCORRECTA", Toast.LENGTH_SHORT).show();
                                showAlertDialogLogin();

                                try {
                                    hideDialog();

                                } catch (Exception e){}

                            }
                            if (!task.isSuccessful()) {
                                Log.d(TAG, "signInWithEmail:onComplete:" + task.getException());
                                //Toast.makeText(getApplication(), "...." + "FALLO EN LA AUTENTICACION", Toast.LENGTH_SHORT).show();
                                showAlertDialogLogin();

                                try {
                                    hideDialog();

                                } catch (Exception e){}

                            } else {
                                try {
                                    hideDialog();

                                } catch (Exception e){
                                    Log.v("Error", e.getLocalizedMessage());
                                }

                                modelo.tipoLogin = "normal";
                                setPreference(modelo.tipoLogin);
                                Intent i = new Intent(Login.this, Inicio.class);
                                i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                                i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
                                i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                                startActivity(i);
                                overridePendingTransition(R.anim.push_down_in, R.anim.push_down_out);
                                finish();

                            }
                        }

                        if (task.getException() instanceof FirebaseNetworkException) {
                            FirebaseNetworkException ex = (FirebaseNetworkException) task.getException();
                            showAlertDialogRed(getApplication(), "" + ex.getLocalizedMessage());


                            try {
                                hideDialog();
                            } catch (Exception e){}
                        }else{
                            try {
                                hideDialog();
                            } catch (Exception e){}
                        }

                    }
                });



    }



    //alerta swit alert
    //alerta
    public void alerta(String titulo,String decripcion){

        new SweetAlertDialog(this, SweetAlertDialog.WARNING_TYPE)
                .setTitleText(titulo)
                .setContentText(decripcion)
                .setConfirmText("Aceptar")
                .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                    @Override
                    public void onClick(SweetAlertDialog sDialog) {

                        sDialog.dismissWithAnimation();

                    }
                })

                .show();
    }

    //posgres dialos sweetalert

    public void loadswet(String text){

        try {
            pDialog = new SweetAlertDialog(this, SweetAlertDialog.PROGRESS_TYPE);
            pDialog.getProgressHelper().setBarColor(Color.parseColor("#A5DC86"));
            pDialog.setTitleText(text);
            pDialog.setCancelable(false);
            pDialog.show();

        }catch (Exception e){

        }

    }


    //oculatomos el dialog
    private void hideDialog() {
        if (pDialog != null)
            pDialog.dismiss();
    }


    public void showAlertDialogLogin() {
        AlertDialog alertDialog = new AlertDialog.Builder(Login.this).create();
        alertDialog.setTitle("");
        alertDialog.setMessage("Por favor verifiqué la información...");
        alertDialog.setButton("OK", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                hideDialog();

            }
        });

        alertDialog.show();
    }

    public void showAlertDialogRed(Context context, String texto) {
        AlertDialog alertDialog = new AlertDialog.Builder(context).create();
        alertDialog.setTitle("Error de red");
        alertDialog.setMessage("No se pudo loguear. Revise conexión a internet y/o que tenga Google play service activo. " + texto);
        alertDialog.setButton("OK", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
            }
        });

        alertDialog.show();
    }

    @Override
    public void validandoUsuarioOK() {
        logueado();
    }

    @Override
    public void validandoUsuarioError() {

        mAuth.signOut();
        Toast.makeText(getApplicationContext(), "Datos Erróneos", Toast.LENGTH_SHORT).show();
        Intent i = new Intent(Login.this, Inicio.class);
        i.putExtra("vistaPosicion", "dos");
        startActivity(i);
        overridePendingTransition(R.anim.push_down_in, R.anim.push_down_out);
    }

    public void logueado() {
        try {
            hideDialog();
        } catch (Exception e){}

        Intent i = new Intent(Login.this, Inicio.class);
        i.putExtra("vistaPosicion", "dos");
        i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
        i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(i);
        overridePendingTransition(R.anim.zoom_back_in, R.anim.zoom_back_out);
        finish();
    }

    public void olvidoPassword(View v){
        Intent i = new Intent(getApplicationContext(), OlvidoSuPassword.class);
        startActivity(i);
        overridePendingTransition(R.anim.push_down_in, R.anim.push_down_out);

    }

    //GPS
    public void mapa(){
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.ACCESS_FINE_LOCATION,}, 1000);
        } else {
            locationStart();
        }


    }

    private void locationStart() {
        LocationManager mlocManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
        Localizacion Local = new Localizacion();
        Local.setMainActivity(this);
        final boolean gpsEnabled = mlocManager.isProviderEnabled(LocationManager.GPS_PROVIDER);
        if (!gpsEnabled) {
            Intent settingsIntent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
            startActivity(settingsIntent);
        }
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.ACCESS_FINE_LOCATION,}, 1000);
            return;
        }
        mlocManager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, 0, 0, (LocationListener) Local);
        mlocManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 0, 0, (LocationListener) Local);
        Toast.makeText(getApplicationContext(),"Localización agregada", Toast.LENGTH_SHORT).show();

    }
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        if (requestCode == 1000) {
            if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                utility.checkPermission(Login.this);
                locationStart();
                return;
            }


        }



    }
    public void setLocation(Location loc) {
        //Obtener la direccion de la calle a partir de la latitud y la longitud
        if (loc.getLatitude() != 0.0 && loc.getLongitude() != 0.0) {
            try {
                Geocoder geocoder = new Geocoder(this, Locale.getDefault());
                List<Address> list = geocoder.getFromLocation(
                        loc.getLatitude(), loc.getLongitude(), 1);
                if (!list.isEmpty()) {
                    Address DirCalle = list.get(0);


                    String dir =  DirCalle.getAddressLine(0);

                    // Toast.makeText(getApplicationContext(),"Mi direccion es "+ dir, Toast.LENGTH_SHORT).show();
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }
    /* Aqui empieza la Clase Localizacion */
    public class Localizacion implements LocationListener {
        Login mainActivity;
        public Login getMainActivity() {
            return mainActivity;
        }
        public void setMainActivity(Login mainActivity) {
            this.mainActivity = mainActivity;
        }
        @Override
        public void onLocationChanged(Location loc) {
            // Este metodo se ejecuta cada vez que el GPS recibe nuevas coordenadas
            // debido a la deteccion de un cambio de ubicacion
            loc.getLatitude();
            loc.getLongitude();


            modelo.latitud = loc.getLatitude();
            modelo.longitud = loc.getLongitude();

            this.mainActivity.setLocation(loc);
        }
        @Override
        public void onProviderDisabled(String provider) {
            // Este metodo se ejecuta cuando el GPS es desactivado

            Toast.makeText(getApplicationContext(),"GPS Desactivado", Toast.LENGTH_SHORT).show();
        }
        @Override
        public void onProviderEnabled(String provider) {
            // Este metodo se ejecuta cuando el GPS es activado

            Toast.makeText(getApplicationContext(),"GPS Activado", Toast.LENGTH_SHORT).show();
        }
        @Override
        public void onStatusChanged(String provider, int status, Bundle extras) {
            switch (status) {
                case LocationProvider.AVAILABLE:
                    Log.d("debug", "LocationProvider.AVAILABLE");
                    break;
                case LocationProvider.OUT_OF_SERVICE:
                    Log.d("debug", "LocationProvider.OUT_OF_SERVICE");
                    break;
                case LocationProvider.TEMPORARILY_UNAVAILABLE:
                    Log.d("debug", "LocationProvider.TEMPORARILY_UNAVAILABLE");
                    break;
            }
        }
    }


    //fin gps


    public void latlong(){

        ActivityCompat.requestPermissions(Login.this,new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, 1);

        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED)
        {
            Toast.makeText(getApplicationContext(),"Gps inactivo", Toast.LENGTH_LONG).show();
            return;
        }else
        {
            /*Se asigna a la clase LocationManager el servicio a nivel de sistema a partir del nombre.*/
            locManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
            loc = locManager.getLastKnownLocation(LocationManager.GPS_PROVIDER);

            if(loc != null){
                modelo.latitud = loc.getLatitude();
                modelo.longitud = loc.getLongitude();
                //tvLatitud.setText(String.valueOf(loc.getLatitude()));
                //tvLongitud.setText(String.valueOf(loc.getLongitude()));
                //tvAltura.setText(String.valueOf(loc.getAltitude()));
                //tvPrecision.setText(String.valueOf(loc.getAccuracy()));
            }

        }
    }




}