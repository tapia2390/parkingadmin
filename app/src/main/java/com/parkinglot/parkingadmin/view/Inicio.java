package com.parkinglot.parkingadmin.view;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffXfermode;
import android.graphics.Rect;
import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.view.Menu;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.snackbar.Snackbar;
import com.google.android.material.navigation.NavigationView;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.parkinglot.parkingadmin.MainActivity;
import com.parkinglot.parkingadmin.R;
import com.parkinglot.parkingadmin.model.Clases.Usuario;
import com.parkinglot.parkingadmin.model.Comandos.ComandoParqueadero;
import com.parkinglot.parkingadmin.model.Comandos.ComandoPerfilFragment;
import com.parkinglot.parkingadmin.model.Modelo;
import com.parkinglot.parkingadmin.model.utility.Utility;
import com.parkinglot.parkingadmin.ui.historial.HistorialFragment;
import com.parkinglot.parkingadmin.ui.home.HomeFragment;
import com.parkinglot.parkingadmin.ui.parqueadero.ParqueaderoFragment;
import com.parkinglot.parkingadmin.ui.perfil.PerfilFragment;

import androidx.annotation.NonNull;
import androidx.core.view.GravityCompat;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;
import androidx.navigation.NavController;
import androidx.navigation.Navigation;
import androidx.navigation.ui.AppBarConfiguration;
import androidx.navigation.ui.NavigationUI;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import cn.pedant.SweetAlert.SweetAlertDialog;

public class Inicio extends AppCompatActivity implements  ComandoPerfilFragment.OnPerfilChangeListener, ComandoParqueadero.OnParqueaderosChangeListener {

    private AppBarConfiguration mAppBarConfiguration;
    ImageView camara1;
    TextView txtnombre;
    Utility utility;
    FirebaseUser user = FirebaseAuth.getInstance().getCurrentUser();
    ComandoPerfilFragment comandoPerfil;
    ComandoParqueadero comandoParqueadero;
    Usuario usuario;
    Modelo modelo = Modelo.getInstance();
    SweetAlertDialog pDialog;
    DrawerLayout drawer;

    private FirebaseAuth mAuth = FirebaseAuth.getInstance();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate( savedInstanceState );
        this.getWindow().setFlags( WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);

        setContentView( R.layout.activity_inicio );
        Toolbar toolbar = findViewById( R.id.toolbar );
        setSupportActionBar( toolbar );



        if (savedInstanceState != null){
            Intent i = new Intent(getApplicationContext(), MainActivity.class);
            startActivity(i);
            finish();
            return;
        }

        camara1 = (ImageView)findViewById(R.id.imageView);
        txtnombre =  (TextView)findViewById(R.id.txtnombreperfil);


        //instanciamos la clase utility
        utility = new Utility();
        usuario =  null;


        FloatingActionButton fab = findViewById( R.id.fab );
        fab.setOnClickListener( new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Snackbar.make( view, "Replace with your own action", Snackbar.LENGTH_LONG )
                        .setAction( "Action", null ).show();
            }
        } );
         drawer = findViewById( R.id.drawer_layout );
        NavigationView navigationView = findViewById( R.id.nav_view );
        // Passing each menu ID as a set of Ids because each
        // menu should be considered as top level destinations.
        mAppBarConfiguration = new AppBarConfiguration.Builder(
                R.id.nav_home, R.id.nav_perfil, R.id.nav_datosparqueadero,R.id.nav_historial)
                .setDrawerLayout( drawer )
                .build();
        NavController navController = Navigation.findNavController( this, R.id.nav_host_fragment );
        NavigationUI.setupActionBarWithNavController( this, navController, mAppBarConfiguration );
        NavigationUI.setupWithNavController( navigationView, navController );


        // Obtiene una referencia al TextView del otro layout, other.xml
      comandoPerfil =  new ComandoPerfilFragment(this);
        comandoParqueadero =  new ComandoParqueadero(this);
        if (utility.estado(getApplicationContext())) {


            Log.v("User" ,  ""+user.getDisplayName());


            loadswet("Cargando la información...");
            comandoPerfil.getUsuario();
            comandoParqueadero.getParqueadero();


        }else {
            alerta("Sin Internet","Valide la conexión a internet");
        }

        utility.checkPermissionCamera(Inicio.this);


        navigationView.setNavigationItemSelectedListener(new NavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(MenuItem menuItem) {
                int id = menuItem.getItemId();


                if(id == R.id.nav_home) {
                    toolbar.setTitle( "Inicio" );
                    loadFragment(new HomeFragment());
                }

                else if(id == R.id.nav_perfil) {
                    toolbar.setTitle( "Perfil" );
                    loadFragment(new PerfilFragment());
                }
                else if(id == R.id.nav_datosparqueadero) {
                    toolbar.setTitle( "Datos Parqueadero" );
                    loadFragment(new ParqueaderoFragment());
                }
               else if(id == R.id.nav_historial) {
                    toolbar.setTitle( "Historial" );
                    loadFragment(new HistorialFragment());
                }
                else if(id == R.id.cerrasecion) {
                    //Toast.makeText( getApplicationContext(), "logout",Toast.LENGTH_LONG ).show();

                    modelo.tipoLogin = "";
                    user =  null;
                    modelo.usuario = null;
                    modelo.parqueadero = null;
                    modelo.uid = "";
                    mAuth.signOut();
                    mAuth.getInstance().signOut();
                    Toast.makeText(getApplicationContext(),  "Cerrarndo sessión", Toast.LENGTH_SHORT).show();
                    Intent i = new Intent(getApplicationContext().getApplicationContext(), Login.class);
                    startActivity(i);
                }
                drawer.closeDrawer(GravityCompat.START);
                return true;
            }
        });

    }


    public void loadFragment(Fragment fragment) {
        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        transaction.replace(R.id.frame_container, fragment);
        transaction.commit();
    }

      @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.


        getMenuInflater().inflate( R.menu.inicio, menu );
        return true;
    }

    @Override
    public boolean onSupportNavigateUp() {
        NavController navController = Navigation.findNavController( this, R.id.nav_host_fragment );
        return NavigationUI.navigateUp( navController, mAppBarConfiguration )
                || super.onSupportNavigateUp();
    }

    //posgres dialos sweetalert

    public void loadswet(String text){

        try {
            pDialog = new SweetAlertDialog(this, SweetAlertDialog.PROGRESS_TYPE);
            pDialog.getProgressHelper().setBarColor( Color.parseColor("#A5DC86"));
            pDialog.setTitleText(text);
            pDialog.setCancelable(false);
            pDialog.show();

        }catch (Exception e){

        }

    }


    //oculatomos el dialog
    private void hideDialog() {
        if (pDialog != null)
            pDialog.dismiss();
    }



    //alerta swit alert
    //alerta
    public void alerta(String titulo,String decripcion){

        new SweetAlertDialog(this, SweetAlertDialog.WARNING_TYPE)
                .setTitleText(titulo)
                .setContentText(decripcion)
                .setConfirmText("Aceptar")
                .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                    @Override
                    public void onClick(SweetAlertDialog sDialog) {

                        sDialog.dismissWithAnimation();

                    }
                })

                .show();
    }


    @Override
    public void onRestoreInstanceState(Bundle savedInstanceState) {

    }

    @Override
    public void cargoUSuario() {

        hideDialog();
        String nombre = modelo.usuario.getNombre();
        Log.v( "Nombre:" , nombre );



    }

    @Override
    public void setUsuarioListener() {
        hideDialog();

        alerta("Actualización","Datos Actualizados");
    }

    @Override
    public void errorSetUsuario() {
        hideDialog();

        alerta("Eror","Error con el regitro");
    }


    //imagen circular
    public static Bitmap getCircularBitmap(Bitmap bitmap) {
        Bitmap output;

        if (bitmap.getWidth() > bitmap.getHeight()) {
            output = Bitmap.createBitmap(bitmap.getHeight(), bitmap.getHeight(), Bitmap.Config.ARGB_8888);
        } else {
            output = Bitmap.createBitmap(bitmap.getWidth(), bitmap.getWidth(), Bitmap.Config.ARGB_8888);
        }

        Canvas canvas = new Canvas(output);

        final int color = 0xff424242;
        final Paint paint = new Paint();
        final Rect rect = new Rect(0, 0, bitmap.getWidth(), bitmap.getHeight());

        float r = 0;

        if (bitmap.getWidth() > bitmap.getHeight()) {
            r = bitmap.getHeight() / 2;
        } else {
            r = bitmap.getWidth() / 2;
        }

        paint.setAntiAlias(true);
        canvas.drawARGB(0, 0, 0, 0);
        paint.setColor(color);
        canvas.drawCircle(r, r, r, paint);
        paint.setXfermode(new PorterDuffXfermode( PorterDuff.Mode.SRC_IN));
        canvas.drawBitmap(bitmap, rect, rect, paint);
        return output;
    }

    public void Logout(){
        try {
            modelo.tipoLogin = "";
            user =  null;
            modelo.uid = "";
            mAuth.signOut();
            mAuth.getInstance().signOut();
            Toast.makeText(getApplication(),  "Cerrarndo sessión", Toast.LENGTH_SHORT).show();
            Intent i = new Intent(getApplicationContext(), MainActivity.class);
            startActivity(i);
            overridePendingTransition(R.anim.zoom_back_in, R.anim.zoom_back_out);
            finish();
        }
        catch (Exception e){}
    }

    @Override
    public void cargoValidarCorreoFirebase() {

    }

    @Override
    public void cargoValidarCorreoFirebaseEroor() {

    }

    @Override
    public void setParqueaderoListener() {

    }

    @Override
    public void errorSetParqueadero() {

    }

    @Override
    public void setParueaderoListener() {

    }

    @Override
    public void errorCreacionParqueadero() {

    }

    @Override
    public void cargoParqueadero() {
        String nombre = modelo.parqueadero.getNombreParqueadero();
        Log.v( "Nombre:" , nombre );
    }

    @Override
    public void addParueaderoListener() {

    }
}