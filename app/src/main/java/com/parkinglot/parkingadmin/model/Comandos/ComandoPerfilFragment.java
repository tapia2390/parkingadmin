package com.parkinglot.parkingadmin.model.Comandos;

import android.util.Log;


import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.parkinglot.parkingadmin.model.Clases.Usuario;
import com.parkinglot.parkingadmin.model.Modelo;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class ComandoPerfilFragment {


    Modelo modelo = Modelo.getInstance();
    FirebaseDatabase database = FirebaseDatabase.getInstance();
    private FirebaseAuth mAuth = FirebaseAuth.getInstance();
    DatabaseReference referencia = database.getReference();



    //interface del listener de la actividad interesada
    private OnPerfilChangeListener mListener;

    /**
     * Interfaz para avisar de eventos a los interesados
     */
    public interface OnPerfilChangeListener {


        void cargoUSuario();
        void setUsuarioListener();
        void errorSetUsuario();

    }

    public ComandoPerfilFragment(OnPerfilChangeListener mListener){

        this.mListener = mListener;

    }

    public  void actualizarUsuario(String nombre, String apellido,String celular){
        //creating a new user
        //creating a new user

        //String uid = mAuth.getCurrentUser().getUid();

        final DatabaseReference ref = database.getReference("usuario/"+modelo.uid+"/");//ruta path

        Map<String, Object> enviarRegistroUsuario = new HashMap<String, Object>();

        enviarRegistroUsuario.put("nombre", nombre);
        enviarRegistroUsuario.put("apellido", apellido);
        enviarRegistroUsuario.put("celular", celular);



        ref.updateChildren(enviarRegistroUsuario, new DatabaseReference.CompletionListener() {
            @Override
            public void onComplete(DatabaseError databaseError, DatabaseReference databaseReference) {
                if (databaseError == null) {

                    mListener.setUsuarioListener();
                    return;
                } else {
                    mListener.errorSetUsuario();
                }
            }
        });
    }



    public void getUsuario(){
        //metodo que me trae la razon social
        //se guarda en clase modelo

        DatabaseReference ref = database.getReference("usuario/" + modelo.uid);//ruta path
        ref.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot snap) {

                try {

                    modelo.usuario.setNombre(snap.child("nombre").getValue().toString());
                    modelo.usuario.setApellido(snap.child("apellido").getValue().toString());
                    modelo.usuario.setCelular(snap.child("celular").getValue().toString());
                    modelo.usuario.setCorreo(snap.child("correo").getValue().toString());
                    //modelo.usuario.setToken(snap.child("token").getValue().toString());


                    mListener.cargoUSuario();
                }catch (Exception e){
                    Log.v( "error", e.getMessage() );
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                Log.v("Error :X",""+databaseError.getMessage());

                mListener.setUsuarioListener();
            }
        });


    }





    /**
     * Para evitar nullpointerExeptions
     */
    private static OnPerfilChangeListener sDummyCallbacks = new OnPerfilChangeListener()
    {

        @Override
        public void cargoUSuario()
        {}

        @Override
        public void setUsuarioListener()
        {}

        @Override
        public void errorSetUsuario()
        {}


    };
}
