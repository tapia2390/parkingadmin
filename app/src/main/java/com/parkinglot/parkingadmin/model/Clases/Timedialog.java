package com.parkinglot.parkingadmin.model.Clases;


import android.app.Dialog;

import android.app.DialogFragment;

import android.app.TimePickerDialog;

import android.os.Bundle;

import android.view.View;

import android.widget.TextView;

import android.widget.TimePicker;



import java.util.Calendar;



/**

 * Created by NIGAMASIS on 04-Jan-17.

 */



public class Timedialog extends DialogFragment implements TimePickerDialog.OnTimeSetListener {



    TextView tv;



    public void get(View view)

    {

        tv = (TextView)view;

    }



    @Override

    public Dialog onCreateDialog(Bundle savedInstanceState) {



        final Calendar calendar = Calendar.getInstance();

        int hour = calendar.get(Calendar.HOUR_OF_DAY);

        int minute = calendar.get(Calendar.MINUTE);




        return new TimePickerDialog(getActivity(),this,hour,minute,false);

    }



    @Override

    public void onTimeSet(TimePicker view, int hourofday, int minute) {


        String am_pm = "";

        Calendar datetime = Calendar.getInstance();
        datetime.set(Calendar.HOUR_OF_DAY, hourofday);
        datetime.set(Calendar.MINUTE, minute);

        if (datetime.get(Calendar.AM_PM) == Calendar.AM)
            am_pm = "AM";
        else if (datetime.get(Calendar.AM_PM) == Calendar.PM)
            am_pm = "PM";

        String strHrsToShow = (datetime.get(Calendar.HOUR) == 0) ?"12":datetime.get(Calendar.HOUR)+"";


        String time =strHrsToShow+":"+datetime.get(Calendar.MINUTE)+" "+am_pm;

        tv.setText(time);

    }

}