package com.parkinglot.parkingadmin.model.Clases;

public class Parqueadero {

    private String key;
    private String nombreParqueadero;
    private boolean estado;
    private double lat;
    private double lon;
    private String horarioInicio;
    private String horarioFin;
    private double precio;
    private int cupos;
    private int Disponibles;
    private String tipoMoneda;
    private String  formaDeCobro;
    private String uid;



    public Parqueadero() {
    }

    public Parqueadero(String key, String nombreParqueadero, boolean estado, double lat, double lon, String horarioInicio, String horarioFin, double precio, int cupos, int disponibles, String tipoMoneda, String formaDeCobro, String uid) {
        this.key = key;
        this.nombreParqueadero = nombreParqueadero;
        this.estado = estado;
        this.lat = lat;
        this.lon = lon;
        this.horarioInicio = horarioInicio;
        this.horarioFin = horarioFin;
        this.precio = precio;
        this.cupos = cupos;
        Disponibles = disponibles;
        this.tipoMoneda = tipoMoneda;
        this.formaDeCobro = formaDeCobro;
        this.uid = uid;
    }

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public String getNombreParqueadero() {
        return nombreParqueadero;
    }

    public void setNombreParqueadero(String nombreParqueadero) {
        this.nombreParqueadero = nombreParqueadero;
    }

    public boolean getEstado() {
        return estado;
    }

    public void setEstado(boolean estado) {
        this.estado = estado;
    }

    public double getLat() {
        return lat;
    }

    public void setLat(double lat) {
        this.lat = lat;
    }

    public double getLon() {
        return lon;
    }

    public void setLon(double lon) {
        this.lon = lon;
    }

    public String getHorarioInicio() {
        return horarioInicio;
    }

    public void setHorarioInicio(String horarioInicio) {
        this.horarioInicio = horarioInicio;
    }

    public String getHorarioFin() {
        return horarioFin;
    }

    public void setHorarioFin(String horarioFin) {
        this.horarioFin = horarioFin;
    }

    public double getPrecio() {
        return precio;
    }

    public void setPrecio(double precio) {
        this.precio = precio;
    }

    public int getCupos() {
        return cupos;
    }

    public void setCupos(int cupos) {
        this.cupos = cupos;
    }

    public int getDisponibles() {
        return Disponibles;
    }

    public void setDisponibles(int disponibles) {
        Disponibles = disponibles;
    }

    public String getTipoMoneda() {
        return tipoMoneda;
    }

    public void setTipoMoneda(String tipoMoneda) {
        this.tipoMoneda = tipoMoneda;
    }

    public String getformaDeCobro() {
        return formaDeCobro;
    }

    public void setformaDeCobro(String formaDeCobro) {
        this.formaDeCobro = formaDeCobro;
    }

    public String getUid() {
        return uid;
    }

    public void setUid(String uid) {
        this.uid = uid;
    }
}
