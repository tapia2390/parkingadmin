package com.parkinglot.parkingadmin.model.Clases;

public class Vehiculo {


    private  String placa;
    private  String modelo;
    private  String tarjetaPropiedad;

    public Vehiculo(){}

    public Vehiculo(String placa, String modelo, String tarjetaPropiedad) {
        this.placa = placa;
        this.modelo = modelo;
        this.tarjetaPropiedad = tarjetaPropiedad;
    }

    public String getPlaca() {
        return placa;
    }

    public void setPlaca(String placa) {
        this.placa = placa;
    }

    public String getModelo() {
        return modelo;
    }

    public void setModelo(String modelo) {
        this.modelo = modelo;
    }

    public String getTarjetaPropiedad() {
        return tarjetaPropiedad;
    }

    public void setTarjetaPropiedad(String tarjetaPropiedad) {
        this.tarjetaPropiedad = tarjetaPropiedad;
    }
}
