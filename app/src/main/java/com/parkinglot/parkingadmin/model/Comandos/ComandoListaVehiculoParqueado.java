package com.parkinglot.parkingadmin.model.Comandos;



import android.util.Log;

import androidx.annotation.NonNull;

import com.firebase.client.annotations.Nullable;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;
import com.parkinglot.parkingadmin.model.Clases.HistoricoVehiculoParqueado;
import com.parkinglot.parkingadmin.model.Clases.VehiculoParqueado;
import com.parkinglot.parkingadmin.model.Modelo;

import java.util.HashMap;
import java.util.Map;


/**
 * Created by tacto on 2/10/17.
 */

public class ComandoListaVehiculoParqueado {

    Modelo modelo = Modelo.getInstance();
    FirebaseDatabase database = FirebaseDatabase.getInstance();
    private FirebaseAuth mAuth = FirebaseAuth.getInstance();


    //interface del listener de la actividad interesada
    private OnValidarListaVehiculoParqueadoChangeListener mListener;

    public ComandoListaVehiculoParqueado(OnValidarListaVehiculoParqueadoChangeListener mListener){

        this.mListener = mListener;

    }

    /**
     * Interfaz para avisar de eventos a los interesados
     */
    public interface OnValidarListaVehiculoParqueadoChangeListener {

        void validandoListaVehiculoParqueadoOK();
        void validandoListaVehiculoParqueadoError();


    }

    public interface OnFinalizarOrden {
        void finalizo(VehiculoParqueado orden);
        void fallo(VehiculoParqueado orden);
    }






    public void  historialListaVhiculoParqueado(){
        //preguntasFrecuentes
        modelo.listHistoricoVehiculoParqueado.clear();
        DatabaseReference ref = database.getReference("HistorialVehiculoParqueado/"+modelo.uidParqueadero);//ruta path
        Query query = ref.orderByChild("uidParqueadero").equalTo(modelo.uidParqueadero);
        query.addChildEventListener(new ChildEventListener() {
            @Override
            public void onChildAdded(@NonNull DataSnapshot snFav, @Nullable String s) {
                HistoricoVehiculoParqueado vehiculoParqueado = new HistoricoVehiculoParqueado();
                Long timestamp =  (Long) snFav.child("timestamp").getValue();
                Long timestampfin =  (Long) snFav.child("timestampfin").getValue();

                double cobro =  Double.parseDouble(  snFav.child("cobro").getValue().toString() );
                int  fraccion =  Integer.parseInt(  snFav.child("fraccion").getValue().toString() );
                int  tiempoDeParqueo =  Integer.parseInt(  snFav.child("tiempoDeParqueo").getValue().toString() );

                vehiculoParqueado.setKey(snFav.getKey());
                vehiculoParqueado.setUidParqueadero(snFav.child("uidParqueadero").getValue().toString());
                vehiculoParqueado.setNombreParqueadero(snFav.child("nombreParqueadero").getValue().toString());
                vehiculoParqueado.setPlaca(snFav.child("placa").getValue().toString());
                vehiculoParqueado.setFecha(snFav.child("fecha").getValue().toString());
                vehiculoParqueado.setEstado(snFav.child("estado").getValue().toString());

                vehiculoParqueado.setCobro(cobro);
                vehiculoParqueado.setFraccion(fraccion);
                vehiculoParqueado.setTimestamp(timestamp);
                vehiculoParqueado.setMoneda(snFav.child("moneda").getValue().toString());
                vehiculoParqueado.setPlaca(snFav.child("placa").getValue().toString());
                vehiculoParqueado.setTiempoDeParqueo(tiempoDeParqueo);
                vehiculoParqueado.setTimestampFin(timestampfin);

                modelo.listHistoricoVehiculoParqueado.add(vehiculoParqueado);

                mListener.validandoListaVehiculoParqueadoOK();

            }

            @Override
            public void onChildChanged(@NonNull DataSnapshot dataSnapshot, @Nullable String s) {

                Log.v("dataSnapshot", "1");
            }

            @Override
            public void onChildRemoved(@NonNull DataSnapshot dataSnapshot) {
                Log.v("dataSnapshot", "2");
            }

            @Override
            public void onChildMoved(@NonNull DataSnapshot dataSnapshot, @Nullable String s) {
                Log.v("dataSnapshot", "3");
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {
                Log.v("dataSnapshot", "4");
            }
        });


        if (modelo.listVehiculoParqueado.size() ==0){
            mListener.validandoListaVehiculoParqueadoOK();
        }
    }




    public void  getListaVhiculoParqueado(){
        //preguntasFrecuentes
        modelo.listVehiculoParqueado.clear();
        DatabaseReference ref = database.getReference("VehiculoParqueado/"+modelo.uidParqueadero);//ruta path
        Query query = ref.orderByChild("uidParqueadero").equalTo(modelo.uidParqueadero);
        query.addChildEventListener(new ChildEventListener() {
            @Override
            public void onChildAdded(@NonNull DataSnapshot snFav, @Nullable String s) {
                VehiculoParqueado vehiculoParqueado = new VehiculoParqueado();
                Long timestamp =  (Long) snFav.child("timestamp").getValue();

                vehiculoParqueado.setKey(snFav.getKey());
                vehiculoParqueado.setUidParqueadero(snFav.child("uidParqueadero").getValue().toString());
                vehiculoParqueado.setNombreParqueadero(snFav.child("nombreParqueadero").getValue().toString());
                vehiculoParqueado.setPlaca(snFav.child("placa").getValue().toString());
                vehiculoParqueado.setFecha(snFav.child("fecha").getValue().toString());
                vehiculoParqueado.setEstado(snFav.child("estado").getValue().toString());
                vehiculoParqueado.setTimestamp(timestamp);



                modelo.listVehiculoParqueado.add(vehiculoParqueado);

                mListener.validandoListaVehiculoParqueadoOK();

            }

            @Override
            public void onChildChanged(@NonNull DataSnapshot dataSnapshot, @Nullable String s) {

                Log.v("dataSnapshot", "1");
            }

            @Override
            public void onChildRemoved(@NonNull DataSnapshot dataSnapshot) {
                Log.v("dataSnapshot", "2");
            }

            @Override
            public void onChildMoved(@NonNull DataSnapshot dataSnapshot, @Nullable String s) {
                Log.v("dataSnapshot", "3");
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {
                Log.v("dataSnapshot", "4");
            }
        });


        if (modelo.listVehiculoParqueado.size() ==0){
            mListener.validandoListaVehiculoParqueadoOK();
        }
    }




    public  void updateListaVhiculoParqueado(double cobrar, long min, double _tiempo,String moneda, String placa,  long timestampfin, double _minutes,String key){

        final DatabaseReference ref = database.getReference("VehiculoParqueado/"+modelo.uidParqueadero+"/"+key);//ruta path

        Map<String, Object> enviarRegistroUsuario = new HashMap<String, Object>();

        enviarRegistroUsuario.put("cobro", cobrar);
        enviarRegistroUsuario.put("moneda", moneda);
        enviarRegistroUsuario.put("placa", placa);
        enviarRegistroUsuario.put("fraccion", min);
        enviarRegistroUsuario.put("tiempoDeParqueo", _minutes);
        enviarRegistroUsuario.put("timestampfin", timestampfin);
        enviarRegistroUsuario.put("estado", "FINALIZADO");


        ref.updateChildren(enviarRegistroUsuario, new DatabaseReference.CompletionListener() {
            @Override
            public void onComplete(DatabaseError databaseError, DatabaseReference databaseReference) {
                if (databaseError == null) {

                   moverOrdenAlHistorico( key );
                    return;
                } else {
                    mListener.validandoListaVehiculoParqueadoError();
                }
            }
        });
    }


    public void moverOrdenAlHistorico(String key) {
        modelo = Modelo.getInstance();

        //colocar metodo update fecha y hora de llegada


        DatabaseReference ref = database.getReference("VehiculoParqueado/"+modelo.uidParqueadero+"/"+key);//ruta path
        DatabaseReference refHistorico = database.getReference("HistorialVehiculoParqueado/"+modelo.uidParqueadero+"/"+key);//ruta path

        moverRegistro(ref, refHistorico, key);

    }


    public void moverRegistro(final DatabaseReference fromPath, final DatabaseReference toPath, final String idOrden) {
        fromPath.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                modelo = Modelo.getInstance();
                toPath.setValue(dataSnapshot.getValue(), new DatabaseReference.CompletionListener() {
                    @Override
                    public void onComplete(DatabaseError error, DatabaseReference ref) {
                        if (error != null) {
                            VehiculoParqueado orden = modelo.getOrden(idOrden);
                            mListener.validandoListaVehiculoParqueadoOK();
                        } else {
                            fromPath.removeValue();
                            VehiculoParqueado orden = modelo.getOrden(idOrden);
                            modelo.eliminarOrden(idOrden);
                            mListener.validandoListaVehiculoParqueadoError();

                        }
                    }
                });


            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                mListener.validandoListaVehiculoParqueadoError();


                Log.d("firebaseError", "error intentando copiar datos historico");
            }
        });
    }

    /**
     * Para evitar nullpointerExeptions
     */
    private static OnValidarListaVehiculoParqueadoChangeListener sDummyCallbacks = new OnValidarListaVehiculoParqueadoChangeListener()
    {
        @Override
        public void validandoListaVehiculoParqueadoOK()
        {}


        @Override
        public void validandoListaVehiculoParqueadoError()
        {}





    };

}