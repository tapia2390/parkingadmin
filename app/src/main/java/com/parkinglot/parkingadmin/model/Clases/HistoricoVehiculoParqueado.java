package com.parkinglot.parkingadmin.model.Clases;

public class HistoricoVehiculoParqueado {

    String key;
    double cobro;
    String estado;
    String nombreParqueadero;
    String placa;
    int fraccion;
    int tiempoDeParqueo;
    String moneda;
    String fecha;
    Long timestamp;
    Long timestampFin;
    String uidParqueadero;

    public HistoricoVehiculoParqueado(){}

    public HistoricoVehiculoParqueado(String key, double cobro, String estado, String nombreParqueadero, String placa, int fraccion, int tiempoDeParqueo, String moneda, String fecha, Long timestamp, Long timestampFin, String uidParqueadero) {
        this.key = key;
        this.cobro = cobro;
        this.estado = estado;
        this.nombreParqueadero = nombreParqueadero;
        this.placa = placa;
        this.fraccion = fraccion;
        this.tiempoDeParqueo = tiempoDeParqueo;
        this.moneda = moneda;
        this.fecha = fecha;
        this.timestamp = timestamp;
        this.timestampFin = timestampFin;
        this.uidParqueadero = uidParqueadero;
    }

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public double getCobro() {
        return cobro;
    }

    public void setCobro(double cobro) {
        this.cobro = cobro;
    }

    public String getEstado() {
        return estado;
    }

    public void setEstado(String estado) {
        this.estado = estado;
    }

    public String getNombreParqueadero() {
        return nombreParqueadero;
    }

    public void setNombreParqueadero(String nombreParqueadero) {
        this.nombreParqueadero = nombreParqueadero;
    }

    public String getPlaca() {
        return placa;
    }

    public void setPlaca(String placa) {
        this.placa = placa;
    }

    public int getFraccion() {
        return fraccion;
    }

    public void setFraccion(int fraccion) {
        this.fraccion = fraccion;
    }

    public int getTiempoDeParqueo() {
        return tiempoDeParqueo;
    }

    public void setTiempoDeParqueo(int tiempoDeParqueo) {
        this.tiempoDeParqueo = tiempoDeParqueo;
    }

    public String getMoneda() {
        return moneda;
    }

    public void setMoneda(String moneda) {
        this.moneda = moneda;
    }

    public String getFecha() {
        return fecha;
    }

    public void setFecha(String fecha) {
        this.fecha = fecha;
    }

    public Long getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(Long timestamp) {
        this.timestamp = timestamp;
    }

    public Long getTimestampFin() {
        return timestampFin;
    }

    public void setTimestampFin(Long timestampFin) {
        this.timestampFin = timestampFin;
    }

    public String getUidParqueadero() {
        return uidParqueadero;
    }

    public void setUidParqueadero(String uidParqueadero) {
        this.uidParqueadero = uidParqueadero;
    }
}
