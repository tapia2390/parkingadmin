package com.parkinglot.parkingadmin.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.parkinglot.parkingadmin.R;
import com.parkinglot.parkingadmin.model.Clases.HistoricoVehiculoParqueado;
import com.parkinglot.parkingadmin.model.Modelo;

import java.util.ArrayList;
import java.util.List;


public class ListVehiculoHistorialAdapter extends RecyclerView.Adapter<ListVehiculoHistorialAdapter.HistoricoVehiculoParqueadoViewHolder> implements Filterable {

    private Context context;
    private List<HistoricoVehiculoParqueado> listVehiculos;
    private List<HistoricoVehiculoParqueado> listaHistoricoVehiculoParqueado;
    Modelo modelo = Modelo.getInstance();


    public ListVehiculoHistorialAdapter(Context context, List<HistoricoVehiculoParqueado> listVehiculos) {
        super();
        this.context = context;
        this.listVehiculos = listVehiculos;
        this.listaHistoricoVehiculoParqueado = listVehiculos;


    }

    @NonNull
    @Override
    public HistoricoVehiculoParqueadoViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(context).inflate( R.layout.adapter_vehiculo_historial, viewGroup, false);
        return new HistoricoVehiculoParqueadoViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull HistoricoVehiculoParqueadoViewHolder holder, final int position) {
        String nombreParwueadero = modelo.parqueadero.getNombreParqueadero();
        String fecha = listaHistoricoVehiculoParqueado.get(position).getFecha();
        String placa = listaHistoricoVehiculoParqueado.get(position).getPlaca();
        String estado = listaHistoricoVehiculoParqueado.get(position).getEstado();
        String tiempo =  modelo.parqueadero.getformaDeCobro();
        String moneda =  modelo.parqueadero.getTipoMoneda();
        double precio = modelo.parqueadero.getPrecio();
        int duracion =  listaHistoricoVehiculoParqueado.get(position).getTiempoDeParqueo();
        double cancelo =  listaHistoricoVehiculoParqueado.get(position).getCobro();



        holder.txtparqueadero.setText(nombreParwueadero );
        holder.txtfecha.setText(fecha);
        holder.txtplaca.setText(placa);
        holder.txtestado.setText("Estado: " +estado);
        holder.tiempo.setText(tiempo );
        holder.valorcobro.setText( "Fracción: "+precio +" "+ moneda  );
        holder.duracion.setText("Duración: "+  duracion+" Minutos"  );
        holder.cancelo.setText(  "Cancelo: "+ cancelo+" "+ moneda  );







    }

    @Override
    public int getItemCount() {
        return listaHistoricoVehiculoParqueado.size();
    }

    /**
     * <p>Returns a filter that can be used to constrain data with a filtering
     * pattern.</p>
     *
     * <p>This method is usually implemented by {@link RecyclerView.Adapter}
     * classes.</p>
     *
     * @return a filter used to constrain data
     */
    @Override
    public Filter getFilter() {
        return new Filter() {
            @Override
            protected FilterResults performFiltering(CharSequence constraint) {
                String charSequenceString = constraint.toString();
                if (charSequenceString.isEmpty()) {
                    listaHistoricoVehiculoParqueado = listVehiculos;
                } else {
                    List<HistoricoVehiculoParqueado> filteredList = new ArrayList<>();
                    for (HistoricoVehiculoParqueado name : listVehiculos) {
                        if (name.getPlaca().toLowerCase().contains(charSequenceString.toLowerCase())) {
                            filteredList.add(name);
                        }
                        listaHistoricoVehiculoParqueado = filteredList;
                    }

                }
                FilterResults results = new FilterResults();
                results.values = listaHistoricoVehiculoParqueado;
                return results;
            }

            @Override
            protected void publishResults(CharSequence constraint, FilterResults results) {
                listaHistoricoVehiculoParqueado = (List<HistoricoVehiculoParqueado>) results.values;
                notifyDataSetChanged();
            }
        };
    }




    class HistoricoVehiculoParqueadoViewHolder extends RecyclerView.ViewHolder {

        private TextView txtfecha;
        private TextView txtparqueadero;
        private TextView txtplaca;
        private TextView txtestado;
        private TextView valorcobro;
        private TextView tiempo;
        private TextView duracion;
        private TextView cancelo;


        HistoricoVehiculoParqueadoViewHolder(@NonNull View itemView) {
            super(itemView);
            txtfecha = itemView.findViewById(R.id.txtfecha);
            txtparqueadero = itemView.findViewById(R.id.txtparqueadero);
            txtplaca = itemView.findViewById(R.id.txtplaca);
            txtestado = itemView.findViewById(R.id.txtestado);
            valorcobro = itemView.findViewById(R.id.valorcobro);
            tiempo = itemView.findViewById(R.id.tiempo);
            duracion = itemView.findViewById(R.id.duracion);
            cancelo = itemView.findViewById(R.id.cancelo);

        }
    }

}