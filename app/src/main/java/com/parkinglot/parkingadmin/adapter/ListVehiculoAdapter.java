package com.parkinglot.parkingadmin.adapter;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.google.firebase.database.ServerValue;
import com.parkinglot.parkingadmin.R;
import com.parkinglot.parkingadmin.model.Clases.VehiculoParqueado;
import com.parkinglot.parkingadmin.model.Comandos.ComandoListaVehiculoParqueado;
import com.parkinglot.parkingadmin.model.Modelo;
import com.parkinglot.parkingadmin.model.utility.Utility;

import org.joda.time.Instant;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import cn.pedant.SweetAlert.SweetAlertDialog;


public class ListVehiculoAdapter extends RecyclerView.Adapter<ListVehiculoAdapter.VehiculoParqueadoViewHolder> implements Filterable,
        ComandoListaVehiculoParqueado.OnValidarListaVehiculoParqueadoChangeListener {

    private Context context;
    private List<VehiculoParqueado> listVehiculos;
    private List<VehiculoParqueado> listaVehiculoParqueado;
    Modelo modelo = Modelo.getInstance();
    ComandoListaVehiculoParqueado  comandoListaVehiculoParqueado;
    Utility utility;


    public ListVehiculoAdapter(Context context, List<VehiculoParqueado> listVehiculos) {
        super();
        this.context = context;
        this.listVehiculos = listVehiculos;
        this.listaVehiculoParqueado = listVehiculos;
        utility = new Utility();
        comandoListaVehiculoParqueado = new ComandoListaVehiculoParqueado( this );
    }

    @NonNull
    @Override
    public VehiculoParqueadoViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(context).inflate( R.layout.adapter_vehiculo_parqueadero, viewGroup, false);
        return new VehiculoParqueadoViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull VehiculoParqueadoViewHolder holder, final int position) {
        String key =listaVehiculoParqueado.get(position).getKey();
        String nombreParwueadero = modelo.parqueadero.getNombreParqueadero();
        String fecha = listaVehiculoParqueado.get(position).getFecha();
        String placa = listaVehiculoParqueado.get(position).getPlaca();
        String estado = listaVehiculoParqueado.get(position).getEstado();
        String _tiempo =  modelo.parqueadero.getformaDeCobro();
        String moneda =  modelo.parqueadero.getTipoMoneda();
        Long timestampInicio = listaVehiculoParqueado.get(position).getTimestamp();
        double precio = modelo.parqueadero.getPrecio();



        holder.txtparqueadero.setText(nombreParwueadero );
        holder.txtfecha.setText(fecha);
        holder.txtplaca.setText(placa);
        holder.txtestado.setText(estado);
        holder.tiempo.setText(_tiempo);
        holder.valorcobro.setText( precio +" "+ moneda  );





        holder.btnfinalizar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {


                //String timestampfin = new java.text.SimpleDateFormat("MM/dd/yyyy h:mm:ss a").format(new Date());

                long timestampIncio=  timestampInicio;
                Instant instant = Instant.now();
                long timestampfin = instant.getMillis();

                Log.v("timestamp",timestampIncio+"----"+timestampfin);

                Long tiempo =(timestampfin-timestampIncio);

                Log.v( "res:" , ""+tiempo );
              /*  modelo.VehiculoParqueado = listaVehiculoParqueado.get(position);
                Intent i = new Intent(context, DetalleVehiculoParqueado.class);
                i.putExtra("VehiculoParqueado","VehiculoParqueado");
                context.startActivity(i);*/

                long minutes = (tiempo / 1000)  / 60;

                String[] parts = _tiempo.split(" ");
                String part1 = parts[0]; // 123
                String part2 = parts[1];

                //valor fraccion
                Long valorFraccion = Long.parseLong(part1);

                double _minutes = Double.parseDouble( ""+minutes );
                double _min = Double.parseDouble( ""+valorFraccion );

                double res = (_minutes/_min);
                Log.v( "res:" , ""+res );
                double total = (res * precio);
                Log.v( "total:" , ""+total );
                int _cobrar = (int) Math.round(total);
                double cobrar = Double.parseDouble( ""+_cobrar );
                Log.v( "roundDbl:" , ""+cobrar + " "+moneda);
                String pagar = cobrar + " "+moneda;
                alerta("Valor a cobrar",cobrar,valorFraccion,res,moneda,placa,timestampfin,_minutes,key);

                //  int seconds = (int)((tiempo / 1000) % 60);

            }
        });

    }


    //alerta
    public void alerta(String titulo, double cobrar,long valorFraccion, double _tiempo, String moneda, String placa,   long timestampfin, double _minutes,String key){

        new SweetAlertDialog(context, SweetAlertDialog.WARNING_TYPE)
                .setTitleText(titulo)
                .setContentText(cobrar+" "+ moneda + " " + placa)
                .setConfirmText("Aceptar")
                .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                    @Override
                    public void onClick(SweetAlertDialog sDialog) {
                        if (utility.estado(context)) {
                            comandoListaVehiculoParqueado.updateListaVhiculoParqueado( cobrar,  valorFraccion, _tiempo, moneda,  placa,   timestampfin,  _minutes,key );
                        }else {
                            alerta("Sin Internet","Valide la conexión a internet");
                        }

                        sDialog.dismissWithAnimation();

                    }
                })
                .setCancelButton("Cancelar", new SweetAlertDialog.OnSweetClickListener() {
                    @Override
                    public void onClick(SweetAlertDialog sDialog) {
                        sDialog.dismissWithAnimation();
                    }
                })

                .show();
    }

    @Override
    public int getItemCount() {
        return listaVehiculoParqueado.size();
    }

    /**
     * <p>Returns a filter that can be used to constrain data with a filtering
     * pattern.</p>
     *
     * <p>This method is usually implemented by {@link RecyclerView.Adapter}
     * classes.</p>
     *
     * @return a filter used to constrain data
     */
    @Override
    public Filter getFilter() {
        return new Filter() {
            @Override
            protected FilterResults performFiltering(CharSequence constraint) {
                String charSequenceString = constraint.toString();
                if (charSequenceString.isEmpty()) {
                    listaVehiculoParqueado = listVehiculos;
                } else {
                    List<VehiculoParqueado> filteredList = new ArrayList<>();
                    for (VehiculoParqueado name : listVehiculos) {
                        if (name.getPlaca().toLowerCase().contains(charSequenceString.toLowerCase())) {
                            filteredList.add(name);
                        }
                        listaVehiculoParqueado = filteredList;
                    }

                }
                FilterResults results = new FilterResults();
                results.values = listaVehiculoParqueado;
                return results;
            }

            @Override
            protected void publishResults(CharSequence constraint, FilterResults results) {
                listaVehiculoParqueado = (List<VehiculoParqueado>) results.values;
                notifyDataSetChanged();
            }
        };
    }

    @Override
    public void validandoListaVehiculoParqueadoOK() {

        notifyDataSetChanged();
    }

    @Override
    public void validandoListaVehiculoParqueadoError() {
        notifyDataSetChanged();
    }


    class VehiculoParqueadoViewHolder extends RecyclerView.ViewHolder {

        private TextView txtfecha;
        private TextView txtparqueadero;
        private TextView txtplaca;
        private TextView txtestado;
        private TextView valorcobro;
        private TextView tiempo;
        private Button btnfinalizar;


        VehiculoParqueadoViewHolder(@NonNull View itemView) {
            super(itemView);
            txtfecha = itemView.findViewById(R.id.txtfecha);
            txtparqueadero = itemView.findViewById(R.id.txtparqueadero);
            txtplaca = itemView.findViewById(R.id.txtplaca);
            txtestado = itemView.findViewById(R.id.txtestado);
            valorcobro = itemView.findViewById(R.id.valorcobro);
            tiempo = itemView.findViewById(R.id.tiempo);
            btnfinalizar = itemView.findViewById(R.id.btnfinalizar);

        }
    }


    //alerta
    public void alerta(String titulo,String decripcion){

        new SweetAlertDialog(context, SweetAlertDialog.WARNING_TYPE)
                .setTitleText(titulo)
                .setContentText(decripcion)
                .setConfirmText("Aceptar")
                .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                    @Override
                    public void onClick(SweetAlertDialog sDialog) {

                        sDialog.dismissWithAnimation();

                    }
                })

                .show();
    }
}